#include "BigMath.h"

gmp_randstate_t BigMath::rand;

void BigMath::init() {
	gmp_randinit_default(rand);
	gmp_randseed_ui(rand, clock());
}


BigMath::BigMath()
{
}

mpz_ptr BigMath::createBigInt() {
	return createBigIntUi(0);
}

mpz_ptr BigMath::cloneBigInt(mpz_ptr value) {
	mpz_ptr result = (mpz_ptr)malloc(sizeof(mpz_t));

	mpz_init(result);
	mpz_set(result, value);

	return result;
}

mpz_ptr BigMath::createBigIntUi(uint value) {
	mpz_ptr result = (mpz_ptr)malloc(sizeof(mpz_t));

	mpz_init(result);
	mpz_set_ui(result, value);

	return result;
}

mpz_ptr BigMath::createBigIntSi(signed int value) {
	mpz_ptr result = (mpz_ptr)malloc(sizeof(mpz_t));

	mpz_init(result);
	mpz_set_si(result, value);

	return result;
}

void BigMath::freeBigInt(mpz_ptr ptr) {
	mpz_clear(ptr);
	free(ptr);
}

void BigMath::fastPow(mpz_ptr dest, mpz_ptr a, mpz_ptr n) {
	fastPowMod(dest, a, n, NULL);
}

void BigMath::fastPowMod(mpz_ptr dest, mpz_ptr a, mpz_ptr n, mpz_ptr mod) {
	mpz_ptr tmpRes = createBigInt();
	mpz_ptr tmp = cloneBigInt(a);
	mpz_ptr one = createBigIntUi(1);
	mpz_ptr myN = cloneBigInt(n);

	mpz_set_ui(dest, 1);

	uint bits = 0;

	do {
		mpz_and(tmpRes, myN, one);
		if (mpz_cmp_ui(tmpRes, 1) == 0) {
			for (uint k = 0; k < bits; k++) {
				mpz_mul(tmp, tmp, tmp);
				if (mod != NULL) {
					mpz_mod(tmp, tmp, mod);
				}
			}
			bits = 0;
			mpz_mul(dest, dest, tmp);
			if (mod != NULL) {
				mpz_mod(dest, dest, mod);
			}
		}
		mpz_div_ui(myN, myN, 2);
		bits++;
	} while (mpz_cmp_ui(myN, 0) != 0);

	freeBigInt(tmpRes);
	freeBigInt(tmp);
	freeBigInt(one);
	freeBigInt(myN);
}

bool BigMath::isPrime(mpz_ptr value) {
	return _primeCheck(value) && _primeCheck(value) && _primeCheck(value);
}

bool BigMath::_primeCheck(mpz_ptr value) {
	mpz_ptr a = createBigInt();
	mpz_urandomb(a, rand, PRIME_TEST_EXP);

	mpz_ptr p = cloneBigInt(value);
	mpz_sub_ui(p, p, 1);
	mpz_div_ui(p, p, 2);

	mpz_ptr result = createBigInt();
	fastPowMod(result, a, p, value);

	bool ret = mpz_cmp_si(result, 1) == 0;
	mpz_sub(result, result, value); // check if we are at rest class -1
	ret |= mpz_cmp_si(result, -1) == 0;

	freeBigInt(a);
	freeBigInt(p);
	freeBigInt(result);

	return ret;
}

void BigMath::initSieve(eratosthenes* sieve) {
	bool* values = (bool*)malloc(sizeof(bool) * MAX_SIEVE);
	for (uint i = 0; i < MAX_SIEVE; i++) {
		values[i] = true;
	}
	const uint until = MAX_SIEVE_ROOT; // = square root of MAX_SIEVE

	uint i = 2;
	bool next = true;
	while (next) {
		for (uint j = 2; i * j < MAX_SIEVE; j++) {
			values[i * j] = false;
		}

		next = false;
		for (uint j = i + 1; j <= until; j++) {
			if (values[j]) {
				i = j;
				next = true;
				break;
			}
		}
	}

	// now that we finished sieving, build the eratosthenes
	prime_el* curr = sieve;
	curr->value = 2;
	curr->next = NULL;
	for (uint i = 3; i < MAX_SIEVE; i++) {
		if (values[i]) {
			prime_el* tmp = (prime_el*)malloc(sizeof(prime_el));
			tmp->value = i;
			tmp->next = NULL;

			curr->next = tmp;
			curr = tmp;
		}
	}

	free(values);
}

void BigMath::nextPrimes(eratosthenes* sieve, bigPrimes* result, mpz_ptr n) {
	mpz_ptr tmpResult = createBigInt();

	struct _result {
		bool prime;
		mpz_ptr value;
	};

	struct _result* arr = (struct _result*)malloc(sizeof(struct _result) * MAX_BIG_PRIMES);
	for (int i = 0; i < MAX_BIG_PRIMES; i++) {
		arr[i].prime = true;
		arr[i].value = createBigInt();
		if (i == 0) {
			mpz_set(arr[i].value, n);
		}
		else {
			mpz_add_ui(arr[i].value, arr[i - 1].value, 1);
		}
	}

	prime_el* curr = sieve;
	while (curr != NULL) {
		int offset = 0;
		mpz_mod_ui(tmpResult, arr[offset].value, curr->value);
		uint m = mpz_get_ui(tmpResult);
		if (m != 0) {
			offset = curr->value - m + offset;
		}

		while (offset < MAX_BIG_PRIMES) {
			arr[offset].prime = false;
			offset += curr->value;
		}

		curr = curr->next;
	}

	// while building the result, dont forget to free the unused big ints!
	big_prime_el* res = result;
	res->value = NULL;
	for (int i = 0; i < MAX_BIG_PRIMES; i++) {
		if (arr[i].prime) {
			if (res->value == NULL) {
				//first one
				res->value = arr[i].value;
				res->next = NULL;
			}
			else {
				big_prime_el* next = (big_prime_el*)malloc(sizeof(big_prime_el));
				next->next = NULL;
				next->value = arr[i].value;
				res->next = next;
				res = next;
			}
		}
		else {
			freeBigInt(arr[i].value);
		}
	}

	freeBigInt(tmpResult);
	free(arr);
}

bool BigMath::nextSecurePrime(eratosthenes* sieve, mpz_ptr dest, mpz_ptr n) {
	bigPrimes primes;
	nextPrimes(sieve, &primes, n);
	big_prime_el* el = &primes;
	mpz_ptr q;

	if (el->value == NULL) {
		return false;
	}

	do {
		if (el == NULL) {
			return false;
		}
		q = el->value;
		el = el->next;
	} while (!isPrime(q));

	// Check if the prime is a Sophie-Germain prime
	bool found = false;
	while (!found) {
		mpz_ptr p = cloneBigInt(q);
		mpz_mul_ui(p, p, 2);
		mpz_add_ui(p, p, 1);

		if (isPrime(p)) {
			found = true;
			mpz_set(dest, p);
		}
		else {
			do {
				if (el == NULL) {
					return false;
				}
				q = el->value;
				el = el->next;
			} while (!isPrime(q));
		}

		freeBigInt(p);
	}

	freeBigPrimes(&primes);

	return true;
}

void BigMath::freeSieve(eratosthenes* sieve) {
	prime_el* curr = sieve->next;

	while (curr != NULL) {
		prime_el* tmp = curr;
		curr = tmp->next;
		free(tmp);
	}
}

void BigMath::freeBigPrimes(bigPrimes* primes) {
	freeBigInt(primes->value);
	big_prime_el* curr = primes->next;

	while (curr != NULL) {
		big_prime_el* tmp = curr;
		if (tmp->value != NULL) {
			freeBigInt(tmp->value);
		}
		curr = tmp->next;
		free(tmp);
	}
}