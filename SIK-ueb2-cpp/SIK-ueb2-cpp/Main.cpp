#include "Main.h"

void main(char** args, int argv) {
	BigMath::init();
	SIKueb2cpp::Main frame;
	frame.ShowDialog();
}

void SIKueb2cpp::Main::messageReceived(char* str)
{
	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew String(str));
	}
	else {
		txtArea->AppendText(gcnew String(str));
	}
}

void SIKueb2cpp::Main::handshakeDone()
{
	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("Handshake Done!\r\n"));
	}
	else {
		txtArea->AppendText("Handshake Done!\r\n");
	}
}

void SIKueb2cpp::Main::primeReceived(mpz_ptr prime)
{
	char* buf = new char[mpz_sizeinbase(prime, 10) + 2];
	mpz_get_str(buf, 10, prime);

	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("Prime: "));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String(buf));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("\r\n"));
	}
	else {
		txtArea->AppendText("Prime: ");
		txtArea->AppendText(gcnew System::String(buf));
		txtArea->AppendText("\r\n");
	}
}

void SIKueb2cpp::Main::dhParamReceived(mpz_ptr param)
{
	char* buf = new char[mpz_sizeinbase(param, 10) + 2];
	mpz_get_str(buf, 10, param);
	

	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("dh Param: "));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String(buf));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("\r\n"));
	}
	else {
		txtArea->AppendText("dh Param: ");
		txtArea->AppendText(gcnew System::String(buf));
		txtArea->AppendText("\r\n");
	}
	
}

void SIKueb2cpp::Main::dhParamSend(mpz_ptr param)
{
	char* buf = new char[mpz_sizeinbase(param, 10) + 2];
	mpz_get_str(buf, 10, param);


	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("dh Param Send: "));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String(buf));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("\r\n"));
	}
	else {
		txtArea->AppendText("dh Param Send: ");
		txtArea->AppendText(gcnew System::String(buf));
		txtArea->AppendText("\r\n");
	}

}

void SIKueb2cpp::Main::keyCalculated(mpz_ptr key)
{
	char* buf = new char[mpz_sizeinbase(key, 10) + 2];
	mpz_get_str(buf, 10, key);

	if (txtArea->InvokeRequired) {
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("Key: "));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String(buf));
		txtArea->Invoke(gcnew _printText(this, &SIKueb2cpp::Main::printText), gcnew System::String("\r\n"));
	}
	else {
		txtArea->AppendText("Key: ");
		txtArea->AppendText(gcnew System::String(buf));
		txtArea->AppendText("\r\n");
	}
}
