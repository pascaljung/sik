#pragma once

#include "BigMath.h"
#include "Crypto.h"
#include "MessageCallback.h"
#include "SecureSocket.h"

namespace SIKueb2cpp {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	delegate void _printText(String^ text);

	/// <summary>
	/// Summary for Main
	/// </summary>
	public ref class Main : public System::Windows::Forms::Form, public MessageCallback
	{
	public:
		Main(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	public: void printText(String^ text) {
		this->txtArea->AppendText(text);
	}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Main()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::RichTextBox^  txtArea;
	private: System::Windows::Forms::Button^  btnAlice;
	private: System::Windows::Forms::Button^  btnBob;
	private: System::Windows::Forms::TextBox^  txtMessage;
	private: System::Windows::Forms::Button^  btnSend;
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->txtArea = (gcnew System::Windows::Forms::RichTextBox());
			this->btnAlice = (gcnew System::Windows::Forms::Button());
			this->btnBob = (gcnew System::Windows::Forms::Button());
			this->txtMessage = (gcnew System::Windows::Forms::TextBox());
			this->btnSend = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// txtArea
			// 
			this->txtArea->Location = System::Drawing::Point(12, 41);
			this->txtArea->Name = L"txtArea";
			this->txtArea->Size = System::Drawing::Size(718, 440);
			this->txtArea->TabIndex = 0;
			this->txtArea->Text = L"";
			// 
			// btnAlice
			// 
			this->btnAlice->Location = System::Drawing::Point(12, 12);
			this->btnAlice->Name = L"btnAlice";
			this->btnAlice->Size = System::Drawing::Size(75, 23);
			this->btnAlice->TabIndex = 1;
			this->btnAlice->Text = L"Alice";
			this->btnAlice->UseVisualStyleBackColor = true;
			this->btnAlice->Click += gcnew System::EventHandler(this, &Main::btnAlice_Click);
			// 
			// btnBob
			// 
			this->btnBob->Location = System::Drawing::Point(93, 12);
			this->btnBob->Name = L"btnBob";
			this->btnBob->Size = System::Drawing::Size(75, 23);
			this->btnBob->TabIndex = 2;
			this->btnBob->Text = L"Bob";
			this->btnBob->UseVisualStyleBackColor = true;
			this->btnBob->Click += gcnew System::EventHandler(this, &Main::btnBob_Click);
			// 
			// txtMessage
			// 
			this->txtMessage->Location = System::Drawing::Point(12, 487);
			this->txtMessage->Name = L"txtMessage";
			this->txtMessage->Size = System::Drawing::Size(637, 20);
			this->txtMessage->TabIndex = 3;
			// 
			// btnSend
			// 
			this->btnSend->Location = System::Drawing::Point(655, 487);
			this->btnSend->Name = L"btnSend";
			this->btnSend->Size = System::Drawing::Size(75, 23);
			this->btnSend->TabIndex = 4;
			this->btnSend->Text = L"Send";
			this->btnSend->UseVisualStyleBackColor = true;
			this->btnSend->Click += gcnew System::EventHandler(this, &Main::btnSend_Click);
			// 
			// Main
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(742, 519);
			this->Controls->Add(this->btnSend);
			this->Controls->Add(this->txtMessage);
			this->Controls->Add(this->btnBob);
			this->Controls->Add(this->btnAlice);
			this->Controls->Add(this->txtArea);
			this->Name = L"Main";
			this->Text = L"Main";
			this->Load += gcnew System::EventHandler(this, &Main::Main_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Main_Load(System::Object^  sender, System::EventArgs^  e) {
		/*mpz_ptr r = BigMath::createBigInt();
		mpz_ptr r2 = BigMath::createBigInt();
		mpz_urandomb(r, BigMath::rand, 32);

		byte* p = new byte[12];
		size_t size = 0;

		mpz_export(p, &size, 1, 1, 1, 0, r);
		mpz_import(r2, 4, 1, 1, 1, 0, p);
		
		int cmp = mpz_cmp(r, r2);
		size = cmp;

		clock_t curr = clock();

		mpz_ptr max = BigMath::createBigIntUi(2);
		mpz_ptr p = BigMath::createBigIntUi(512);
		BigMath::fastPow(max, max, p);

		eratosthenes sieve;
		BigMath::initSieve(&sieve);

		bool res = BigMath::nextSecurePrime(&sieve, p, max);
		if (!res) {
			txtArea->AppendText("Not found\r\n");
		}
		else {
			txtArea->AppendText("Result: ");
			char* buf = new char[mpz_sizeinbase(p, 10) + 2];
			mpz_get_str(buf, 10, p);
			txtArea->AppendText(gcnew System::String(buf));
			delete[] buf;
			txtArea->AppendText("\r\n");
		}

		gmp_randstate_t rand;
		gmp_randinit_default(rand);
		gmp_randseed_ui(rand, clock());

		mpz_ptr g = BigMath::createBigIntUi(2); // 2 is always generator for Sophie-Germain (secure) primes
		mpz_ptr a = BigMath::createBigInt();
		mpz_urandomb(a, rand, 512);
		mpz_ptr b = BigMath::createBigInt();
		mpz_urandomb(b, rand, 512);

		mpz_ptr resa = BigMath::createBigInt();
		mpz_ptr resb = BigMath::createBigInt();

		BigMath::fastPowMod(resa, g, a, p);
		BigMath::fastPowMod(resb, g, b, p);

		BigMath::fastPowMod(resa, resa, b, p);
		BigMath::fastPowMod(resb, resb, a, p);

		if (mpz_cmp(resa, resb) == 0) {
			txtArea->AppendText("Diffie-Hellmann Success!\r\n");
		}

		mpz_ptr maxKey = BigMath::createBigIntUi(2);
		mpz_ptr ex = BigMath::createBigIntUi(128);
		BigMath::fastPow(maxKey, maxKey, ex);
		mpz_mod(resa, resa, maxKey);

		uint* key = (uint*)mpz_export(NULL, NULL, 1, 4, 1, 0, resa);
		uint data[2];
		data[0] = 0x12345678;
		data[1] = 0x98765432;

		Crypto::xtea_encipher(data, key);
		txtArea->AppendText(System::Int32(data[0]).ToString("X2"));
		txtArea->AppendText(" ");
		txtArea->AppendText(System::Int32(data[1]).ToString("X2"));
		txtArea->AppendText("\r\n");

		Crypto::xtea_decipher(data, key);
		txtArea->AppendText(System::Int32(data[0]).ToString("X2"));
		txtArea->AppendText(" ");
		txtArea->AppendText(System::Int32(data[1]).ToString("X2"));
		txtArea->AppendText("\r\n");*/
	}

	private: SecureSocket^ s;

private: System::Void btnAlice_Click(System::Object^  sender, System::EventArgs^  e) {
	btnAlice->Enabled = false;
	btnBob->Enabled = false;

	s = gcnew SecureSocket(this);

	mpz_ptr max = BigMath::createBigIntUi(2);
	mpz_ptr p = BigMath::createBigIntUi(512);
	BigMath::fastPow(max, max, p);

	eratosthenes sieve;
	BigMath::initSieve(&sieve);

	bool res = BigMath::nextSecurePrime(&sieve, p, max);
	BigMath::freeBigInt(max);

	txtArea->AppendText("Prime found!\r\n");

	s->setPrime(p);
	s->listen();

}

private: System::Void btnBob_Click(System::Object^  sender, System::EventArgs^  e) {
	btnAlice->Enabled = false;
	btnBob->Enabled = false;

	s = gcnew SecureSocket(this);
	s->connect();
}

		 // Inherited via MessageCallback
		 public:
		 virtual void messageReceived(char* str);
		 virtual void handshakeDone();
		 virtual void primeReceived(mpz_ptr prime);
		 virtual void dhParamReceived(mpz_ptr param);
		 virtual void dhParamSend(mpz_ptr param);
		 virtual void keyCalculated(mpz_ptr key);

private: System::Void btnSend_Click(System::Object^  sender, System::EventArgs^  e) {
		String^ txt = txtMessage->Text;
		char* bytes = new char[txt->Length];
		for (int i = 0; i < txt->Length; i++) {
			bytes[i] = txt[i];
		}

		bytes[txt->Length] = 0;

		txtArea->AppendText("Me: ");
		txtArea->AppendText(txt);
		txtArea->AppendText("\r\n");
		txtMessage->Clear();
		s->sendEncryptedMessage(bytes);
}
};
}
