#include "SecureSocket.h"



SecureSocket::SecureSocket(MessageCallback^ callbacks) {
	bytesRead = 0;
	handshakeDone = false;
	key = NULL;
	rand = BigMath::createBigInt();
	mpz_urandomb(rand, BigMath::rand, 512);
	this->callbacks = callbacks;
}

SecureSocket::~SecureSocket() {
	free(key);
	BigMath::freeBigInt(rand);
}

void SecureSocket::connect() {
	cl = gcnew Socket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::Tcp);
	cl->Connect(IPAddress::Parse(gcnew String("127.0.0.1")), 9005);

	buffer = gcnew array<byte>(255);
	SocketData^ data = gcnew SocketData();
	data->inst = this;
	data->s = cl;
	cl->BeginReceive(buffer, 0, 255, SocketFlags::None, gcnew AsyncCallback(&SecureSocket::receive), data);
}

void SecureSocket::listen() {

	srv = gcnew Socket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::Tcp);
	srv->Bind(gcnew IPEndPoint(IPAddress::Parse(gcnew String("127.0.0.1")), 9005));
	srv->Listen(1);

	SocketData^ data = gcnew SocketData();
	data->s = srv;
	data->inst = this;
	srv->BeginAccept(gcnew AsyncCallback(&SecureSocket::accept), data);
}

void SecureSocket::accept(IAsyncResult ^ res) {
	SecureSocket^ inst = ((SocketData^)res->AsyncState)->inst;
	inst->cl = inst->srv->EndAccept(res);

	array<byte>^ bytes = gcnew array<byte>(133);
	bytes[0] = 0;

	byte* p = new byte[66] { 0 };
	size_t size = 0;
	mpz_export(p + 1, &size, 1, 1, 1, 0, inst->prime);
	p[0] = size;
	for (int i = 0; i < 66; i++) {
		bytes[i + 1] = p[i];
	}

	p = new byte[66] { 0 };
	mpz_ptr dhParam = BigMath::createBigInt();
	mpz_ptr g = BigMath::createBigIntUi(2);
	BigMath::fastPowMod(dhParam, g, inst->rand, inst->prime);
	BigMath::freeBigInt(g);
	mpz_export(p + 1, &size, 1, 1, 1, 0, dhParam);
	p[0] = size;
	for (int i = 0; i < 66; i++) {
		bytes[i + 67] = p[i];
	}

	inst->cl->Send(bytes);

	inst->callbacks->dhParamSend(dhParam);
	BigMath::freeBigInt(dhParam);

	inst->buffer = gcnew array<byte>(255);
	SocketData^ data = gcnew SocketData();
	data->s = inst->cl;
	data->inst = inst;
	inst->cl->BeginReceive(inst->buffer, 0, 255, SocketFlags::None, gcnew AsyncCallback(&SecureSocket::receive), data);
}

void SecureSocket::receive(IAsyncResult ^ res) {
	Socket^ client = ((SocketData^)res->AsyncState)->s;
	SecureSocket^ inst = ((SocketData^)res->AsyncState)->inst;

	inst->bytesRead += client->EndReceive(res);

	if (inst->bytesRead == -1) {
		throw gcnew Exception("Connection closed");
	}

	bool unfinished = false;

	if (!inst->handshakeDone) {
		switch (inst->buffer[0]) {
			case 0:
				// server send us the prime and g^a

				unfinished = !inst->primeDhReceived(client);

				break;
			case 1:
				// other side sent its g^b
				unfinished = !inst->dhReceived();
				break;
		}
	}
	else {
		if (inst->bytesRead % 8 == 0) {
			// all blocks are finished
			inst->decryptMessage();
		}
		else {
			// at least one block is unfinished
			unfinished = true;
		}
	}

	int offset = 0;

	if (unfinished) {
		offset = inst->bytesRead;
	}
	else {
		inst->bytesRead = 0;
	}

	SocketData^ data = gcnew SocketData();
	data->inst = inst;
	data->s = client;
	client->BeginReceive(inst->buffer, offset, 255 - offset, SocketFlags::None, gcnew AsyncCallback(&SecureSocket::receive), data);
}


bool SecureSocket::dhReceived() {
	if (bytesRead != 67) {
		return false;
	}

	byte* p = new byte[66] { 0 };
	for (int i = 0; i < 66; i++) {
		p[i] = buffer[i + 1];
	}

	mpz_ptr dh = BigMath::createBigInt();
	mpz_import(dh, p[0], 1, 1, 1, 0, p + 1);
	delete[] p;
	callbacks->dhParamReceived(dh);

	mpz_ptr k = BigMath::createBigInt();
	BigMath::fastPowMod(k, dh, rand, prime);

	callbacks->keyCalculated(k);

	mpz_ptr maxKey = BigMath::createBigIntUi(2);
	mpz_ptr ex = BigMath::createBigIntUi(128);
	BigMath::fastPow(maxKey, maxKey, ex);
	mpz_mod(k, k, maxKey);

	if (key != NULL) {
		free(key);
	}
	key = (uint*)mpz_export(NULL, NULL, 1, 4, 1, 0, k);
	callbacks->handshakeDone();
	handshakeDone = true;

	BigMath::freeBigInt(dh);
	BigMath::freeBigInt(k);
	BigMath::freeBigInt(maxKey);
	BigMath::freeBigInt(ex);

	return true;
}

bool SecureSocket::primeDhReceived(Socket^ client) {
	if (bytesRead != 133) {
		return false;
	}

	byte* p = new byte[66]{ 0 };
	for (int i = 0; i < 66; i++) {
		p[i] = buffer[i + 1];
	}

	prime = BigMath::createBigInt();
	mpz_import(prime, p[0], 1, 1, 1, 0, p + 1);
	
	callbacks->primeReceived(prime);

	p = new byte[66]{ 0 };
	for (int i = 0; i < 66; i++) {
		p[i] = buffer[i + 67];
	}

	mpz_ptr dh = BigMath::createBigInt();
	mpz_import(dh, p[0], 1, 1, 1, 0, p + 1);
	
	callbacks->dhParamReceived(dh);

	mpz_ptr k = BigMath::createBigInt();
	BigMath::fastPowMod(k, dh, rand, prime);

	callbacks->keyCalculated(k);

	mpz_ptr maxKey = BigMath::createBigIntUi(2);
	mpz_ptr ex = BigMath::createBigIntUi(128);
	BigMath::fastPow(maxKey, maxKey, ex);
	mpz_mod(k, k, maxKey);

	if (key != NULL) {
		free(key);
	}
	key = (uint*)mpz_export(NULL, NULL, 1, 4, 1, 0, k);


	// send our own g^b
	array<byte>^ bytes = gcnew array<byte>(67);
	bytes[0] = 1;

	p = new byte[66] { 0 };
	mpz_ptr dhParam = BigMath::createBigInt();
	mpz_ptr g = BigMath::createBigIntUi(2);
	BigMath::fastPowMod(dhParam, g, rand, prime);
	size_t size = 0;
	mpz_export(p + 1, &size, 1, 1, 1, 0, dhParam);
	p[0] = size;
	for (int i = 0; i < 66; i++) {
		bytes[i + 1] = p[i];
	}


	client->Send(bytes);
	callbacks->dhParamSend(dhParam);
	BigMath::freeBigInt(dhParam);

	callbacks->handshakeDone();
	handshakeDone = true;

	/*BigMath::freeBigInt(dh);
	BigMath::freeBigInt(k);
	BigMath::freeBigInt(g);
	BigMath::freeBigInt(maxKey);
	BigMath::freeBigInt(ex);
	BigMath::freeBigInt(dhParam);*/

	return true;
}

void SecureSocket::decryptMessage() {
	int offset = 0;
	callbacks->messageReceived("Otherside: ");
	while (offset + 8 <= bytesRead) {
		uint* data = new uint[2];
		data[0] = buffer[offset] | (buffer[offset + 1] << 8) | (buffer[offset + 2] << 16) | (buffer[offset + 3] << 24);
		data[1] = buffer[offset + 4] | (buffer[offset + 5] << 8) | (buffer[offset + 6] << 16) | (buffer[offset + 7] << 24);
		offset += 8;

		Crypto::xtea_decipher(data, key);
		char* str = new char[9];
		memset(str, 0, 9);
		str[0] = data[0] & 0xFF;
		str[1] = (data[0] >> 8) & 0xFF;
		str[2] = (data[0] >> 16) & 0xFF;
		str[3] = (data[0] >> 24) & 0xFF;
		str[4] = data[1] & 0xFF;
		str[5] = (data[1] >> 8) & 0xFF;
		str[6] = (data[1] >> 16) & 0xFF;
		str[7] = (data[1] >> 24) & 0xFF;

		callbacks->messageReceived(str);
		//delete[] str; TODO free it anywhere
	}
	callbacks->messageReceived("\r\n");
}

void SecureSocket::sendEncryptedMessage(const char* str) {
	char* toSend;
	int len = strlen(str);
	int mod = (len + 1) % 8;
	if (mod == 0) {
		mod = 8;
	}

	int toSendSize = len + 1 + (8 - mod);
	toSend = new char[toSendSize];
	strncpy(toSend, str, len);
	toSend[len] = 0;

	if (mod != 0) {
		for (int i = 0; i < (8 - mod); i++) {
			toSend[len + 1 + i] = 0;
		}
	}

	array<byte>^ bytes = gcnew array<byte>(toSendSize);
	int offset = 0;
	while (offset + 8 <= toSendSize) {
		uint* data = new uint[2];
		data[0] = toSend[offset] | (toSend[offset + 1] << 8) | (toSend[offset + 2] << 16) | (toSend[offset + 3] << 24);
		data[1] = toSend[offset + 4] | (toSend[offset + 5] << 8) | (toSend[offset + 6] << 16) | (toSend[offset + 7] << 24);

		Crypto::xtea_encipher(data, key);
		bytes[offset + 0] = data[0] & 0xFF;
		bytes[offset + 1] = (data[0] >> 8) & 0xFF;
		bytes[offset + 2] = (data[0] >> 16) & 0xFF;
		bytes[offset + 3] = (data[0] >> 24) & 0xFF;
		bytes[offset + 4] = data[1] & 0xFF;
		bytes[offset + 5] = (data[1] >> 8) & 0xFF;
		bytes[offset + 6] = (data[1] >> 16) & 0xFF;
		bytes[offset + 7] = (data[1] >> 24) & 0xFF;

		offset += 8;
	}

	cl->Send(bytes);
}