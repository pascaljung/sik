#pragma once

#include "BigMath.h"

public class Crypto
{
public:
	static void xtea_encipher(uint v[2], uint const key[4]);
	static void xtea_decipher(uint v[2], uint const key[4]);
private:
	Crypto();
};

