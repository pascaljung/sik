#pragma once

#include "Crypto.h"
#include "MessageCallback.h"

using namespace System;
using namespace System::Net;
using namespace System::Net::Sockets;

ref class SecureSocket
{
public:

	SecureSocket(MessageCallback^ callbacks);
	~SecureSocket();

	void connect();
	void listen();
	void sendEncryptedMessage(const char* str);

	void setPrime(mpz_ptr value) {
		prime = value;
	}

private:
	MessageCallback^ callbacks;
	Socket^ srv;
	Socket^ cl;
	mpz_ptr prime;
	mpz_ptr rand;
	uint* key;
	array<byte>^ buffer;
	int bytesRead;
	bool handshakeDone;

	static void accept(IAsyncResult^ res);
	static void receive(IAsyncResult^ res);

	bool dhReceived();
	bool primeDhReceived(Socket^ client);

	void decryptMessage();
	
};

ref class SocketData {
public:
	Socket^ s;
	SecureSocket^ inst;
};