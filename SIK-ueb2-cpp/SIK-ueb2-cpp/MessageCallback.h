#pragma once

#include "BigMath.h"

public interface class MessageCallback {
public:
	void messageReceived(char* str);
	void handshakeDone();
	void primeReceived(mpz_ptr prime);
	void dhParamReceived(mpz_ptr param);
	void dhParamSend(mpz_ptr param);
	void keyCalculated(mpz_ptr key);
};
