
#include <iostream>

#include "DataTypes.h"
#include "TCPSocket.h"
#include "Server.h"
#include "Client.h"

int main(char** args, int argv) {
	char in[255];
	BigMath::init();

	/*mpz_ptr dest = BigMath::createBigInt();
	mpz_ptr n = BigMath::createBigIntUi(2);
	eratosthenes sieve;

	time_t t = time(NULL);

	BigMath::initSieve(&sieve);
	mpz_pow_ui(n, n, 64);

	BigMath::nextSecurePrime(&sieve, dest, n);
	t = time(NULL) - t;
	mpz_out_str(stdout, 10, dest);
	mpz_set_ui(n, 2);
	mpz_powm_ui(n, n, 1000000, dest);
	cout << endl << t << endl;
	mpz_out_str(stdout, 10, n);

	cin >> in[0];
	return 0;*/

	cout << "Pollard Lambda" << endl << endl;

	cout << "Am I the server(0) or a client(1)? : ";
	char option;
	std::cin.getline(in, 255);
	option = in[0] - 0x30;

	if (option != 0 && option != 1) {
		cout << "I'm sorry, I don't know the option '" << option << "'" << endl << " Over and out!" << endl;
		return 0;
	}

	switch (option) {
		case 0:
			Server::getInstance();
			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Server::acceptThread, &Server::getInstance(), 0, NULL);
			break;
		case 1:
			Client::getInstance();
			CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Client::receiveThread, &Client::getInstance(), 0, NULL);
			break;
	}

	std::cin.getline(in, 255);
}