#ifndef _SERVER_H_
#define _SERVER_H_

#include <iostream>
#include <fstream>

#include "DataTypes.h"
#include "TCPSocket.h"

struct DistinguishedPoint {
	mpz_ptr dp, k, l;
};

class Server {
public:
	~Server();

	static Server& getInstance();

	static DWORD acceptThread(LPVOID inst);
	static DWORD receiveThread(LPVOID inst);

	Server& operator=(Server& other) = delete;
	Server(Server& srv) = delete;

private:
	Server();

	void dpReceived(mpz_ptr dp, mpz_ptr k, mpz_ptr l);
	void computeResult(mpz_ptr y1, mpz_ptr k1, mpz_ptr l1, mpz_ptr y2, mpz_ptr k2, mpz_ptr l2);

	TCPSocket* srv;
	vector<TCPSocket*> clients;
	mpz_ptr g, h, p;
	vector<DistinguishedPoint*> dps;
	HANDLE lock;
	volatile bool solved;
};

struct _srvRcvData {
	Server* inst;
	TCPSocket* client;
};

#endif