#include "TCPSocket.h"

TCPSocket::TCPSocket() : winSock(0) {
	static bool initialized = false;

	if (!initialized) {
		WSADATA wsa;

		if (WSAStartup(MAKEWORD(2, 0), &wsa) == SOCKET_ERROR) {
			throw SocketException("Error initializing WSA!");
		}
	}

	winSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (winSock == INVALID_SOCKET) {
		throw SocketException("Error creating socket!");
	}
}

TCPSocket::TCPSocket(SOCKET s, addr& addr) : winSock(s), peerAddr(addr) {
}

TCPSocket::~TCPSocket() {
}

void TCPSocket::connect(string ip, ushort port) {
	peerAddr.sin_family = AF_INET;
	peerAddr.sin_addr.s_addr = inet_addr(ip.c_str());
	peerAddr.sin_port = htons(port);

	if (::connect(winSock, (sockaddr*)&peerAddr, sizeof(peerAddr)) == SOCKET_ERROR) {
		stringstream str;
		str << "Error connecting to: " << ip << ":" << port;
		throw SocketException(str.str());
	}
}

void TCPSocket::bind(string ip, ushort port) {
	addr bindAddr;
	bindAddr.sin_family = AF_INET;
	bindAddr.sin_addr.s_addr = inet_addr(ip.c_str());
	bindAddr.sin_port = htons(port);

	if (::bind(winSock, (sockaddr*)&bindAddr, sizeof(bindAddr)) == SOCKET_ERROR) {
		stringstream str;
		str << "Unable to bind socket to addr: \"" << ip << ":" << port << "\"";
		throw SocketException(str.str());
	}

	if (listen(winSock, BACKLOG_SIZE) == SOCKET_ERROR) {
		throw SocketException("Error while trying to listen on the bound addr!");
	}
}

TCPSocket* TCPSocket::accept() {
	addr peer;
	int addrSize = sizeof(peer);
	SOCKET s = ::accept(winSock, (sockaddr*)&peer, &addrSize);

	if (s == INVALID_SOCKET) {
		throw SocketException("Error during accept!");
	}

	return new TCPSocket(s, peer);
}

void TCPSocket::send(const Packet& packet) {
	vector<byte> b = packet.getPacket();
	const char* buffer = (char*)b.data();

	if (::send(winSock, buffer, b.size(), 0) == SOCKET_ERROR) {
		throw SocketException("Error sending packet");
	}
}

Packet* TCPSocket::receive() {
	
	Packet* result = NULL;

	while (result == NULL) {
		int size = recv(winSock, buffer, BUFFER_SIZE, 0);

		if (size == 0) {
			// connection closed
			throw SocketException("The peer closed the connection!");
		}
		else if (size < 0) {
			throw SocketException("Error while receiving data!");
		}

		for (int i = 0; i < size; i++) {
			pending.push_back(buffer[i]);
		}

		byte opcode = pending[0];
		switch (opcode) {
			case OP_QUIT:
				result = new Packet(opcode);
				break;
			case OP_START:
			case OP_DP:
				if (pending.size() > 2 * 3) {
					ushort size1 = pending[1] | (pending[2] << 8);
					ushort size2 = pending[3] | (pending[4] << 8);
					ushort size3 = pending[5] | (pending[6] << 8);

					if (pending.size() >= size1 + size2 + size3 + 1 + 2 * 3) { // + opcode + 3 * 2 bytes size
						uint s = size1 + size2 + size3 + 1 + 2 * 3;
						byte* data = new byte[s];
						for (uint i = 0; i < s; i++) {
							data[i] = pending[0];
							pending.pop_front();
						}
						result = new Packet(opcode, size1, size2, size3, (data + 1 + 2 * 3));
						delete data;
					}
				}
				break;
		}
	}

	return result;
}

void TCPSocket::close() {
	closesocket(winSock);
}

const addr& TCPSocket::getAddr() const {
	return peerAddr;
}