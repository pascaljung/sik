#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <iostream>

#include "DataTypes.h"
#include "TCPSocket.h"

#define DP_MASK 0xF

class Client {
public:
	~Client();

	static Client& getInstance();

	static DWORD receiveThread(LPVOID param);

	Client(Client& other) = delete;
	Client& operator=(Client& other) = delete;

private:
	Client();

	static DWORD calculate(LPVOID param);

	TCPSocket* client;
	mpz_ptr g, h, p;
	volatile bool solved;
};

#endif