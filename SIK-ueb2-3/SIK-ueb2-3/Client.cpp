#include "Client.h"



Client::Client() {
	solved = false;
	client = new TCPSocket();

	client->connect("127.0.0.1", 1337);

	std::cout << "Connected!" << endl << "Waiting for problem..." << endl;
}


Client::~Client() {
	delete client;
}

Client& Client::getInstance() {
	static Client cl;

	return cl;
}

DWORD Client::receiveThread(LPVOID param) {
	Client* inst = (Client*)param;

	try {
		while (true) {
			Packet * packet = inst->client->receive();

			if (packet->getOpcode() == OP_START) {
				// Start calculation
				inst->g = BigMath::cloneBigInt(packet->getValue1());
				inst->h = BigMath::cloneBigInt(packet->getValue2());
				inst->p = BigMath::cloneBigInt(packet->getValue3());


				std::cout << "Received problem:" << endl << "g: ";
				mpz_out_str(stdout, 10, inst->g);
				std::cout << endl << "h: ";
				mpz_out_str(stdout, 10, inst->h);
				std::cout << endl << "p: ";
				mpz_out_str(stdout, 10, inst->p);
				std::cout << endl;

				CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&Client::calculate, inst, 0, NULL);
			}
			else {
				// OP_QUIT
				inst->solved = true;
				delete packet;
				break;
			}

			delete packet;
		}
	}
	catch (SocketException& ex) {
		inst->solved = true; // not really solved but we ran into an error
		std::cout << "I experienced a problem with my master: " << ex.what() << endl;
	}

	return 0;
}

DWORD Client::calculate(LPVOID param) {
	Client* inst = (Client*)param;

	mpz_ptr g = inst->g;
	mpz_ptr h = inst->h;
	mpz_ptr p = inst->p;

	// reduce g & h to subgroup
	mpz_mul(g, g, g);
	mpz_mod(g, g, p);

	mpz_mul(h, h, h);
	mpz_mod(h, h, p);

	mpz_ptr y = BigMath::createBigInt();
	mpz_ptr k = BigMath::createBigInt();
	mpz_ptr l = BigMath::createBigInt();

	mpz_ptr m = BigMath::createBigInt();

	mpz_ptr gk = BigMath::createBigInt();
	mpz_ptr hl = BigMath::createBigInt();

	gmp_randseed_ui(BigMath::rand, (uint)time(NULL));

	mpz_urandomb(k, BigMath::rand, 8);
	mpz_urandomb(l, BigMath::rand, 8);

	std::cout << "k = ";
	mpz_out_str(stdout, 10, k);
	std::cout << endl;

	std::cout << "l = ";
	mpz_out_str(stdout, 10, l);
	std::cout << endl;

	BigMath::fastPowMod(gk, g, k, p);
	BigMath::fastPowMod(hl, h, l, p);
	mpz_mul(y, gk, hl);
	mpz_mod(y, y, p);

	while (!inst->solved) {
		mpz_mod_ui(m, y, 3);
		uint mui = mpz_get_ui(m);

		switch (mui) {
			case 0:
				mpz_mul(y, y, g);
				mpz_mod(y, y, p);

				mpz_add_ui(k, k, 1);
				break;
			case 1:
				mpz_mul(y, y, h);
				mpz_mod(y, y, p);

				mpz_add_ui(l, l, 1);
				break;
			case 2:
				mpz_mul(y, y, y);
				mpz_mod(y, y, p);

				mpz_mul_ui(k, k, 2);
				mpz_mul_ui(l, l, 2);
				break;
		}

		if ((mpz_get_ui(y) & DP_MASK) == 0) {
			// least x bits are 0
			inst->client->send(Packet(OP_DP, y, k, l));

			std::cout << "dp reached: ";
			mpz_out_str(stdout, 10, y);
			std::cout << endl;
		}
	}

	BigMath::freeBigInt(hl);
	BigMath::freeBigInt(gk);
	BigMath::freeBigInt(y);
	BigMath::freeBigInt(k);
	BigMath::freeBigInt(l);
	BigMath::freeBigInt(m);

	std::cout << "Fuck yeah, we solved it!" << endl;

	return 0;
}