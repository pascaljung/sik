#ifndef NETWORK_SOCKET_SOCKETEXCEPTION_H_
#define NETWORK_SOCKET_SOCKETEXCEPTION_H_

#include <stdexcept>
#include <string>

using namespace std;

class SocketException : public runtime_error {
public:
	SocketException(const string& msg) : runtime_error(msg) { };
	virtual ~SocketException() throw() { };
};

#endif