#ifndef _PACKET_H_

#include <mpir.h>
#include <vector>

#include "DataTypes.h"
#include "BigMath.h"

#define OP_START 0
#define OP_DP 1
#define OP_QUIT 2

class Packet
{
public:
	Packet(const Packet& p);
	explicit Packet(byte opcode);
	Packet(byte opcode, mpz_ptr value1, mpz_ptr value2, mpz_ptr value3);
	Packet(byte opcode, int size1, int size2, int size3, byte* value);
	~Packet();

	vector<byte> getPacket() const;

	byte getOpcode() const {
		return opcode;
	}

	mpz_ptr getValue1() const {
		return value1;
	}

	mpz_ptr getValue2() const {
		return value2;
	}

	mpz_ptr getValue3() const {
		return value3;
	}

	Packet& operator=(const Packet& p);

private:
	byte opcode;
	mpz_ptr value1;
	mpz_ptr value2;
	mpz_ptr value3;
	bool selfcreated;
};

#endif