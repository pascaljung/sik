#ifndef _DATA_TYPES_H_

using namespace std;

typedef unsigned char byte;
typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned long long ulong;

#endif
