/*
* TCPSocket.h
*
*  Created on: 26.10.2014
*      Author: Pascal
*/

#ifndef _TCP_SOCKET_H_
#define _TCP_SOCKET_H_

#pragma comment(lib, "Ws2_32.lib")

#include <winsock2.h>
#include <sstream>
#include <string>
#include <deque>

#include "DataTypes.h"
#include "Packet.h"
#include "SocketException.h"

typedef sockaddr_in addr;

#define BUFFER_SIZE 256
#define BACKLOG_SIZE 20

class TCPSocket {
public:
	TCPSocket();
	TCPSocket(SOCKET s, addr& addr);
	~TCPSocket();

	void connect(string ip, ushort port);
	void bind(string ip, ushort port);
	TCPSocket* accept();
	void send(const Packet& packet);
	Packet* receive();
	void close();
	const addr& getAddr() const;

	TCPSocket& operator=(TCPSocket&) = delete;
	TCPSocket(const TCPSocket& cpy) = delete;

private:
	deque<byte> pending;
	SOCKET winSock;
	addr peerAddr;
	char buffer[BUFFER_SIZE];
};

#endif