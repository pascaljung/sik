#include "Server.h"



Server::Server() {
	solved = false;
	lock = CreateMutex(NULL, false, NULL);

	srv = new TCPSocket();

	
	std::ifstream problem("problem.txt");


	if (!problem.good()) {
		cout << strerror(errno) << endl;
	}

	char in[1000];
	problem.getline(in, 1000);
	g = BigMath::createBigInt();
	mpz_set_str(g, in, 10);

	if (!problem.good()) {
		cout << "Error reading file" << endl;
	}

	problem.getline(in, 1000);
	h = BigMath::createBigInt();
	mpz_set_str(h, in, 10);

	if (!problem.good()) {
		cout << "Error reading file" << endl;
	}

	problem.getline(in, 1000);
	p = BigMath::createBigInt();
	mpz_set_str(p, in, 10);

	if (!problem.good()) {
		cout << "Error reading file" << endl;
	}

	problem.close();

	cout << "Ahh, I can clearly see the problem now:" << endl << "g: ";
	mpz_out_str(stdout, 10, g);
	cout << endl << "h: ";
	mpz_out_str(stdout, 10, h);
	cout << endl << "p: ";
	mpz_out_str(stdout, 10, p);
	cout << endl;

	srv->bind("127.0.0.1", 1337);
	cout << "Server bound!" << endl;

}

Server::~Server() {
	delete srv;
	BigMath::freeBigInt(g);
	BigMath::freeBigInt(h);
	BigMath::freeBigInt(p);
}

Server & Server::getInstance() {
	static Server srv;

	return srv;
}

DWORD Server::acceptThread(LPVOID param) {
	Server* inst = (Server*)param;
	while (true) {
		TCPSocket* cl = inst->srv->accept();

		inst->clients.push_back(cl);
		cl->send(Packet(OP_START, inst->g, inst->h, inst->p));
		cout << "A dear friend connected. I already provided him with the problem." << endl;

		_srvRcvData* data = new _srvRcvData;
		data->inst = inst;
		data->client = cl;
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Server::receiveThread, data, 0, NULL);
	}

	return 0;
}

DWORD Server::receiveThread(LPVOID param) {
	_srvRcvData* data = (_srvRcvData*)param;
	Server* inst = data->inst;
	TCPSocket* client = data->client;
	delete data;
	try {
		while (true) {
			Packet* p = client->receive();

			if (p->getOpcode() != OP_DP) {
				cout << "Something seems to be wrong with the client as it didn't send a distinguished point..." << endl;
			}

			// Do something with the distinguished point
			if (!inst->solved) {
				inst->dpReceived(p->getValue1(), p->getValue2(), p->getValue3());
			}

			delete p;
		}
	}
	catch (SocketException& ex) {
		cout << "I experienced a problem with one of my friends: " << ex.what() << endl;
	}

	return 0;
}

void Server::dpReceived(mpz_ptr dp, mpz_ptr k, mpz_ptr l) {
	// Lets see if we already have that dp
	WaitForSingleObject(lock, INFINITE);

	for (uint i = 0; i < dps.size(); i++) {
		if (mpz_cmp(dps[i]->dp, dp) == 0) {
			this->computeResult(dps[i]->dp, dps[i]->k, dps[i]->l, dp, k, l);
			break;
		}
	}

	if (!solved) {
		DistinguishedPoint* _dp = new DistinguishedPoint;
		_dp->dp = BigMath::cloneBigInt(dp);
		_dp->k = BigMath::cloneBigInt(k);
		_dp->l = BigMath::cloneBigInt(l);
		dps.push_back(_dp);
	}
	else {
		Packet p(OP_QUIT);

		for (uint i = 0; i < clients.size(); i++) {
			clients[i]->send(p);
		}
	}

	ReleaseMutex(lock);
}

void Server::computeResult(mpz_ptr y1, mpz_ptr k1, mpz_ptr l1, mpz_ptr y2, mpz_ptr k2, mpz_ptr l2) {
	// TODO cleanup bigints...
	mpz_ptr numerator = BigMath::createBigInt();
	mpz_ptr denominator = BigMath::createBigInt();
	mpz_ptr q = BigMath::createBigInt();
	mpz_ptr qsub2 = BigMath::createBigInt();
	mpz_ptr qmul2 = BigMath::createBigInt();
	mpz_ptr lhs= BigMath::createBigInt();
	mpz_ptr rhs = BigMath::createBigInt();
	mpz_ptr erg1 = BigMath::createBigInt();
	mpz_ptr chin = BigMath::createBigInt();
	uint erg2 = 0;

	mpz_sub_ui(q, p, 1);
	mpz_div_ui(q, q, 2);
	mpz_sub_ui(qsub2, q, 2);
	mpz_mul_ui(qmul2, q, 2);

	mpz_sub(numerator, k1, k2);
	mpz_mod(numerator, numerator, q);

	mpz_sub(denominator, l2, l1);
	mpz_mod(denominator, denominator, q);

	BigMath::fastPowMod(erg1, denominator, qsub2, q);
	mpz_mul(erg1, erg1, numerator);
	mpz_mod(erg1, erg1, q);

	BigMath::fastPowMod(lhs, g, q, p);
	BigMath::fastPowMod(rhs, h, q, p);

	if (mpz_cmp(lhs, rhs) == 0) {
		erg2 = 1;
	}
	else if (mpz_cmp_ui(lhs, 1) == 0 || mpz_cmp_ui(rhs, 1) == 0) {
		erg2 = 0;
	}
	else {
		cout << "Cannot solve problem!" << endl;
		return;
	}

	// chinese rest problem (special form: x=erg1*2*2^(q-2) + erg2*q*1 mod (q*2) = erg1*2*2^(q-2) + {0, 1}*q mod (q*2) )
	mpz_mul_ui(erg1, erg1, 2);
	mpz_set_ui(numerator, 2);
	BigMath::fastPowMod(numerator, numerator, qsub2, qmul2);
	mpz_mul(erg1, erg1, numerator);
	mpz_mod(erg1, erg1, qmul2);

	if (erg2 != 0) {
		mpz_add(erg1, erg1, q);
		mpz_mod(erg1, erg1, qmul2);
	}

	stringstream ss;

	ss << "g^";
	char* asStr = mpz_get_str(NULL, 10, k1);
	ss << asStr << " * h^";
	delete[] asStr;
	asStr = mpz_get_str(NULL, 10, l1);
	ss << asStr << " = ";
	delete[] asStr;
	asStr = mpz_get_str(NULL, 10, y1);
	ss << asStr << endl;
	delete[] asStr;

	ss << "g^";
	asStr = mpz_get_str(NULL, 10, k1);
	ss << asStr << " * h^";
	delete[] asStr;
	asStr = mpz_get_str(NULL, 10, l2);
	ss << asStr << " = ";
	delete[] asStr;
	asStr = mpz_get_str(NULL, 10, y2);
	ss << asStr << endl << endl;
	delete[] asStr;

	ss << "Result: Mod(";
	asStr = mpz_get_str(NULL, 10, erg1);
	ss << asStr << ", ";
	delete[] asStr;
	asStr = mpz_get_str(NULL, 10, qmul2);
	ss << asStr << ")" << endl;
	delete[] asStr;


	cout << "Found two matching points!" << endl << ss.str();

	ofstream out;
	out.open("solved.txt", ofstream::out | ofstream::trunc);

	out << ss.str();

	out.flush();
	out.close();

	solved = true;
}