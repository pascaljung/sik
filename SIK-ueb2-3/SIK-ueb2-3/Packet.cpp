#include "Packet.h"



Packet::Packet(byte opcode, mpz_ptr value1, mpz_ptr value2, mpz_ptr value3):
	opcode(opcode), value1(value1), value2(value2), value3(value3), selfcreated(false) {
}

Packet::Packet(byte opcode, int size1, int size2, int size3, byte* value): opcode(opcode), selfcreated(true) {
	this->value1 = BigMath::createBigInt();
	this->value2 = BigMath::createBigInt();
	this->value3 = BigMath::createBigInt();

	mpz_import(this->value1, size1, 1, 1, 1, 0, value);
	mpz_import(this->value2, size2, 1, 1, 1, 0, value + size1);
	mpz_import(this->value3, size3, 1, 1, 1, 0, value + size1 + size3);
}

Packet::Packet(const Packet & p) {
	this->opcode = p.opcode;
	this->value1 = BigMath::cloneBigInt(p.value1);
	this->value2 = BigMath::cloneBigInt(p.value2);
	this->value3 = BigMath::cloneBigInt(p.value3);
	this->selfcreated = true;
}

Packet::Packet(byte opcode): opcode(opcode), value1(NULL), value2(NULL), value3(NULL), selfcreated(false) {
}

Packet::~Packet()
{
	if (selfcreated) {
		BigMath::freeBigInt(this->value1);
		BigMath::freeBigInt(this->value2);
		BigMath::freeBigInt(this->value3);
	}
}

vector<byte> Packet::getPacket() const {
	vector<byte> result;
	result.push_back(opcode);

	if (value1 != NULL) {
		size_t size1, size2, size3;
		byte* values1 = (byte*)mpz_export(NULL, &size1, 1, 1, 1, 0, value1);
		byte* values2 = (byte*)mpz_export(NULL, &size2, 1, 1, 1, 0, value2);
		byte* values3 = (byte*)mpz_export(NULL, &size3, 1, 1, 1, 0, value3);

		result.push_back(size1 & 0xFF);
		result.push_back((size1 >> 8) & 0xFF);

		result.push_back(size2 & 0xFF);
		result.push_back((size2 >> 8) & 0xFF);

		result.push_back(size3 & 0xFF);
		result.push_back((size3 >> 8) & 0xFF);

		for (uint i = 0; i < size1; i++) {
			result.push_back(values1[i]);
		}

		for (uint i = 0; i < size2; i++) {
			result.push_back(values2[i]);
		}

		for (uint i = 0; i < size3; i++) {
			result.push_back(values3[i]);
		}
	}

	return result;
}

Packet & Packet::operator=(const Packet & p) {
	if (this == &p) {
		return *this;
	}

	this->opcode = p.opcode;

	if (selfcreated) {
		BigMath::freeBigInt(this->value1);
		BigMath::freeBigInt(this->value2);
		BigMath::freeBigInt(this->value3);
	}

	this->value1 = BigMath::cloneBigInt(p.value1);
	this->value2 = BigMath::cloneBigInt(p.value2);
	this->value3 = BigMath::cloneBigInt(p.value3);
	selfcreated = true;

	return *this;
}
