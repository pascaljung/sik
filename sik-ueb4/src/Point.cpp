/*
 * Point.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#include "Point.h"

Point::Point(bint x, bint y) {
	this->x = new bint(x);
	this->y = new bint(y);
	this->infinity = false;
}

Point::Point() :
		x(NULL), y(NULL), infinity(true) {
}

void Point::freeValues() {
	if (this->x != NULL) {
		delete this->x;
	}

	if (this->y != NULL) {
		delete this->y;
	}
}

void Point::copyValues(Point& other) {
	if (other.infinity) {
		this->x = NULL;
		this->y = NULL;
		this->infinity = true;
	} else {
		this->x = new bint(*other.x);
		this->y = new bint(*other.y);
		this->infinity = false;
	}
}

Point::~Point() {
	this->freeValues();
}

Point::Point(Point& cpy) {
	this->copyValues(cpy);
}

Point& Point::operator=(Point& other) {
	if (this == &other) {
		return *this;
	}

	this->freeValues();
	this->copyValues(other);

	return *this;
}

void Point::setX(bint* x) {
	if (x == NULL) {
		throw NullPointerException("X may not be NULL!");
	}

	if (this->x == x) {
		// assign same pointer -> do nothing. This is important for later logic
		return;
	}

	if (this->x == NULL) {
		this->infinity = false;
		this->x = new bint(*x);

		if (this->y == NULL) {
			this->y = new bint(0);
		}
	} else {
		delete this->x;
		this->x = new bint(*x);
	}
}

void Point::setY(bint* y) {
	if (y == NULL) {
		throw NullPointerException("Y may not be NULL!");
	}

	if (this->y == y) {
		// assign same pointer -> do nothing. This is important for later logic
		return;
	}

	if (this->y == NULL) {
		this->infinity = false;
		this->y = new bint(*y);

		if (this->x == NULL) {
			this->x = new bint(0);
		}
	} else {
		delete this->y;
		this->y = new bint(*y);
	}
}

void Point::setInfinity() {
	if (!infinity) {
		freeValues();
		infinity = true;
	}
}

bool Point::isValid(EllipticCurve& curve) {
	// TODO implement
	return true;
}

void Point::add(EllipticCurve& curve, Point& other) {
	if (!curve.isValid()) {
		throw InvalidCurveException("Invalid");
	}

	// Case 1, one or both points infinity
	if (this->infinity) {
		copyValues(other);
		return;
	}
	else if (other.infinity) {
		return;
	}

	// Case 2, P1 = -P2
	if (*this->y == -(*other.y)) {
		this->setInfinity();
		return;
	}

	// Case 3
	bint m;
	bint modSub2 = bint(curve.getMod());
	modSub2 -= 2;
	if (*this == other) {
		// Case 3a, P1 = P2
		m = 2 * (*y);
		mpz_powm(m.get_mpz_t(), m.get_mpz_t(), modSub2.get_mpz_t(), curve.getMod().get_mpz_t()); // this calculates 1/2y
		bint numerator = 3 * (*x) * (*x);
		numerator += curve.getA();
		m *= numerator;
	} else {
		// Case 3b, P1 != P2
		m = (*other.x) - (*this->x);
		mpz_powm(m.get_mpz_t(), m.get_mpz_t(), modSub2.get_mpz_t(), curve.getMod().get_mpz_t()); // this calculates 1/x2-x1
		m *= ((*other.y) - (*this->y));
	}

	// calculate points
	bint x1 = *x;
	bint y1 = *y;
	bint ox = *(other.x);
	delete x;
	delete y;
	x = new bint(m * m - x1 - ox);
	mpz_mod(x->get_mpz_t(), x->get_mpz_t(), curve.getMod().get_mpz_t());
	y = new bint(m * (x1 - (*x)) - y1);
	mpz_mod(y->get_mpz_t(), y->get_mpz_t(), curve.getMod().get_mpz_t());
}

void Point::mul(EllipticCurve& curve, bint times) {
	Point doubles = *this;
	while (times != 0) {
		if ((times & 1) == 1) {
			this->add(curve, doubles);
		}

		doubles.add(curve, doubles);
		times = times / 2;
	}
}

bool Point::equals(Point& other) {
	if (this == &other) {
		return true;
	}
	else if (this->infinity && other.infinity) {
		return true;
	}
	else if (*this->x == *other.x && *this->y == *other.y) {
		return true;
	}
	else {
		return false;
	}
}

bool Point::operator==(Point& other) {
	return this->equals(other);
}

bool Point::operator!=(Point& other) {
	return !this->equals(other);
}
