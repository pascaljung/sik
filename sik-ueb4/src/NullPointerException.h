/*
 * NullPointerException.h
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#ifndef SRC_NULLPOINTEREXCEPTION_H_
#define SRC_NULLPOINTEREXCEPTION_H_

#include <stdexcept>
#include <string>

class NullPointerException: public std::logic_error {
public:
	NullPointerException(std::string cause): logic_error(cause) {
	}
	~NullPointerException() { };
};



#endif /* SRC_NULLPOINTEREXCEPTION_H_ */
