/*
 * Ecc.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#include "EllipticCurve.h"

EllipticCurve::EllipticCurve(bint a, bint b, bint mod): a(a), b(b), mod(mod), pointCount(-1) {
}

EllipticCurve::~EllipticCurve() {
}

bool EllipticCurve::isValid() {

	bint result = ((4 * a * a * a) + (27 * b * b)) % mod;
	return result != 0;
}

bint EllipticCurve::computePoints() {
	pointCount = 1; // 1, because of infinity

	bint pmin1half = (mod - 1) / 2;

	// split work into 4 threads
	bint quarter = mod / 4;
	bint* s1 = new bint(0);
	bint* e1 = new bint(quarter);

	bint* s2 = new bint(quarter);
	bint* e2 = new bint(quarter + quarter);

	bint* s3 = new bint(*e2);
	bint* e3 = new bint(*s3 + quarter);

	bint* s4 = new bint(*e3);
	bint* e4 = new bint(*s4 + quarter);

	if (*e4 != mod) {
		*e4 += 1;
	}

	thread t1(&EllipticCurve::compute, this, s1, e1);
	thread t2(&EllipticCurve::compute, this, s2, e2);
	thread t3(&EllipticCurve::compute, this, s3, e3);

	EllipticCurve::compute(this, s4, e4);

	t1.join();
	t2.join();
	t3.join();

	delete s1; delete e1;
	delete s2; delete e2;
	delete s3; delete e3;
	delete s4; delete e4;

	return pointCount;
}

bint EllipticCurve::getPointsCount() {
	return pointCount;
}

void EllipticCurve::compute(EllipticCurve* curve, bint* start, bint* end) {
	bint& a = curve->a;
	bint& b = curve->b;
	bint& mod = curve->mod;

	bint counter = 0;

	bint pmin1half = (mod - 1) / 2;

	 for (bint i = *start; i < *end; i++) {
                bint ysquare = ((i * i * i) + (a * i) + b) % mod;

                if (ysquare == 0) {
                        // y = 0
                        counter++;
                } else {
                        bint yqr;
                        mpz_powm(yqr.get_mpz_t(), ysquare.get_mpz_t(), pmin1half.get_mpz_t(), mod.get_mpz_t());

                        if (yqr == 1) {
                                // has qr
                                counter += 2;
                        }
                }
	}

	// TODO set a lock!
	curve->pointCount += counter;
}

string EllipticCurve::toString() {
	stringstream ss;
	ss << "Curve: y² = x³ + (" << a.get_mpz_t() << ")*x + (" << b.get_mpz_t() << ") mod " << mod.get_mpz_t();

	return ss.str();
}
