/*
 * Application.h
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#ifndef SRC_APPLICATION_H_
#define SRC_APPLICATION_H_

#include <iostream>
#include <ctime>
#include <gmpxx.h>

#include "Point.h"
#include "EllipticCurve.h"

using namespace std;

class Application {
public:
	Application();
	virtual ~Application();

	void compute();
};

#endif /* SRC_APPLICATION_H_ */
