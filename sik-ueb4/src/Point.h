/*
 * Point.h
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#ifndef SRC_POINT_H_
#define SRC_POINT_H_

#include <sstream>
#include <string>
#include <stdexcept>

#include <gmpxx.h>

#include "EllipticCurve.h"
#include "NullPointerException.h"

using namespace std;

typedef mpz_class bint;

class Point {
public:
	Point();
	Point(bint x, bint y);
	~Point();

	Point(Point& cpy);
	Point& operator=(Point& other);
	bool operator==(Point& other);
	bool operator!=(Point& other);

	bint* getX() {
		return x;
	}

	bint* getY() {
		return y;
	}

	bool isInfinity() {
		return this->infinity;
	}

	void setX(bint* x);

	void setY(bint* y);

	void setInfinity();

	bool isValid(EllipticCurve& curve);

	void add(EllipticCurve& curve, Point& other);
	void mul(EllipticCurve& curve, bint times);

	string toString() {
		stringstream ss;
		if (this->infinity) {
			ss << "(Infinity)";
		} else {
			ss << "(" << x->get_mpz_t() << ", " << y->get_mpz_t() << ")";
		}

		return ss.str();
	}

private:
	bint* x;
	bint* y;
	bool infinity;

	void freeValues();
	void copyValues(Point& other);
	bool equals(Point& other);
};

#endif /* SRC_POINT_H_ */
