/*
 * Ecc.h
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#ifndef SRC_EllipticCURVE_H_
#define SRC_EllipticCURVE_H_

#include <string>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <thread>

using namespace std;

#include <gmpxx.h>

typedef mpz_class bint;

class EllipticCurve {
public:
	EllipticCurve(bint a, bint b, bint mod);
	~EllipticCurve();

	bool isValid();

	bint& getA() {
		return a;
	}

	bint& getB() {
		return b;
	}

	bint& getMod() {
		return mod;
	}

	bint computePoints();
	bint getPointsCount();

	string toString();

	static void compute(EllipticCurve* curve, bint* start, bint* end);

private:
	bint a;
	bint b;
	bint mod;
	bint pointCount;
};

class InvalidCurveException: public runtime_error {
public:
	InvalidCurveException(string msg): runtime_error(msg) {	}
	~InvalidCurveException() { }
};

#endif /* SRC_EllipticCURVE_H_ */
