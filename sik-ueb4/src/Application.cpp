/*
 * Application.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: pjung
 */

#include "Application.h"

Application::Application() {

}

Application::~Application() {
}

void Application::compute() {
	Point p1(2, -4);
	Point p2(5, -2);

	char a[1000];
        cout << "mod: "; cin.getline(a, 1000);

	EllipticCurve curve(1, 1, bint(a, 10));
	/*char b[1000];
	char mod[1000];

	cout << "a: "; cin.getline(a, 1000);
	cout << "b: "; cin.getline(b, 1000);
	cout << "mod: "; cin.getline(mod, 1000);

	EllipticCurve curve(bint(a, 10), bint(b, 10), bint(mod, 10));

	if (curve.isValid()) {
		cout << curve.toString() << " is valid!" << endl;
	}
	else {
		cout << curve.toString() << " is invalid!" << endl;
		return;
	}

	time_t timer = time(NULL);
	cout << curve.toString() << " has " << curve.computePoints() << " Points." << endl;
	cout << "Calculation needed " << difftime(time(NULL), timer) << " seconds" << endl;*/

	p1.mul(curve, bint(a, 10));
	cout << p1.toString() << endl;
}
