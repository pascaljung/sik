// SIK-ueb2.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <time.h>

#include "Math.h"
#include "Cryptrography.h"

int main() {

	clock_t curr = clock();

	mpz_ptr max = createBigIntUi(2);
	mpz_ptr p = createBigIntUi(512);
	fastPow(max, max, p);

	eratosthenes sieve;
	initSieve(&sieve);

	bool res = nextSecurePrime(&sieve, p, max);
	if (!res) {
		printf("Not found\r\n");
	}
	else {
		printf("Result: "); mpz_out_str(stdout, 10, p); printf("\r\n");
	}

	gmp_randstate_t rand;
	gmp_randinit_default(rand);
	gmp_randseed_ui(rand, clock());

	mpz_ptr g = createBigIntUi(2); // 2 is always generator for Sophie-Germain (secure) primes
	mpz_ptr a = createBigInt();
	mpz_urandomb(a, rand, 512);
	mpz_ptr b = createBigInt();
	mpz_urandomb(b, rand, 512);

	mpz_ptr resa = createBigInt();
	mpz_ptr resb = createBigInt();

	fastPowMod(resa, g, a, p);
	fastPowMod(resb, g, b, p);

	fastPowMod(resa, resa, b, p);
	fastPowMod(resb, resb, a, p);

	if (mpz_cmp(resa, resb) == 0) {
		printf("Diffie-Hellmann Success!\r\n");
	}

	mpz_ptr maxKey = createBigIntUi(2);
	mpz_ptr e = createBigIntUi(128);
	fastPow(maxKey, maxKey, e);
	mpz_mod(resa, resa, maxKey);

	uint* key = mpz_export(NULL, NULL, 1, 4, 1, 0, resa);
	uint* data = malloc(sizeof(uint) * 2);
	data[0] = 123456789;
	data[1] = 987654321;
	xtea_encipher(data, key);
	xtea_decipher(data, key);
	free(data);
	free(key);


	/*bigPrimes result;
	result.next = NULL;
	result.value = NULL;
	nextPrimes(&sieve, &result, a);

	unsigned int counter = 0;
	big_prime_el* el = &result;
	while (el != NULL) {
		if (el->value != NULL && isPrime(el->value)) {
			counter++;
		}
		el = el->next;
	}

	printf("%d\r\n", counter);*/
	printf("%d\r\n", clock() - curr);
	scanf("");

	return 0;

/*	mpz_ptr a = createBigIntUi(2);
	mpz_ptr tmp = createBigIntUi(512);
	fastPow(a, a, tmp);
	nextSecurePrime(tmp, a);

	return 0;

	mpz_ptr p = createBigIntUi(7);
	mpz_inp_str(p, stdin, 10);

	printf("%d\r\n", isPrime(p));
	system("pause");

	return 0;

	mpz_ptr result = createBigInt();
	mpz_ptr a = createBigIntUi(2);
	mpz_ptr tmp = createBigIntUi(256);
	fastPow(a, a, tmp);
	mpz_ptr n = createBigIntUi(210);
	mpz_ptr mod = createBigIntUi(2);
	mpz_set_ui(tmp, 512);
	fastPow(mod, mod, tmp);

	clock_t curr = clock();

	fastPow(result, a, n);

	
	//printf("Result: "); mpz_out_str(stdout, 10, result); printf("\r\n");
	printf("%d\r\n", (clock() - curr));

	curr = clock();
	fastPowMod(result, a, n, mod);
	//printf("Result: "); mpz_out_str(stdout, 10, result); printf("\r\n");

	printf("%d\r\n", (clock() - curr));

	scanf("");

	mpz_clear(a);
	mpz_clear(n);
	mpz_clear(mod);

    return 0;*/
}