#ifndef __CRYPTOGRAPHY_H_
#define __CRYPTOGRAPHY_H_

#include "Math.h"

void xtea_encipher(uint v[2], uint const key[4]);
void xtea_decipher(uint v[2], uint const key[4]);


#endif
