#ifndef __MATH_H_
#define __MATH_H_

#include <mpir.h>
#include <time.h>

typedef unsigned int uint;
typedef unsigned char bool;
#define true 1
#define false 0

#define PRIME_TEST_EXP 8
#define MAX_SIEVE 1000000
#define MAX_SIEVE_ROOT 1000
#define MAX_BIG_PRIMES 100000

typedef struct _prime_el {
	unsigned int value;
	struct _prime_el* next;

} prime_el;

typedef prime_el eratosthenes;

typedef struct _big_prime_el {
	mpz_ptr value;
	struct _big_prime_el* next;

} big_prime_el;

typedef big_prime_el bigPrimes;

/**
 * Create a new big integer. Call freeBigInt() to release the memory again!
*/
mpz_ptr createBigInt();
/**
 * Clone the given bin integer. Call freeBigInt() to release the memory again!
*/
mpz_ptr cloneBigInt(mpz_ptr value);
/**
 * Create a new big integer with the given unsigned integer value. Call freeBigInt() to release the memory again!
*/
mpz_ptr createBigIntUi(unsigned int value);
/**
 * Create a new big integer with the given signed integer value. Call freeBigInt() to release the memory again!
*/
mpz_ptr createBigIntSi(signed int value);

/**
 * Release the allocated memory and cleanup the variable
*/
void freeBigInt(mpz_ptr ptr);

/**
 * Fast implementation of a^n by multiple squaring. The result is stored in dest
*/
void fastPow(mpz_ptr dest, mpz_ptr a, mpz_ptr n);
/**
 * Fast implementation of a^n mod <mod> by multiple squaring and consequently keeping the result small (modulu after each step)
*/
void fastPowMod(mpz_ptr dest, mpz_ptr a, mpz_ptr n, mpz_ptr mod);

/**
 * Check if the given value is a prime number by a probabilistic check. The check is done three times but may deliver a wrong result on bad coincidences!
*/
bool isPrime(mpz_ptr value);
/**
 * Probabilistic prime test. May deliver a wrong result on bad coincidences!
*/
bool _primeCheck(mpz_ptr value);

void initSieve(eratosthenes* sieve);
void nextPrimes(eratosthenes* sieve, bigPrimes* result, mpz_ptr n);
bool nextSecurePrime(eratosthenes* sieve, mpz_ptr dest, mpz_ptr n);
void freeSieve(eratosthenes* sieve);
void freeBigPrimes(bigPrimes* primes);

#endif